﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ITShopCore;
using ITShopСore.Cards;

namespace UnitTestITShop
{
    [TestClass]
    public class IT_Shop_Test1
    {

        [TestMethod]
        public void TestMethod1()
        {
            Shop s = new Shop();
            s.RegiserCustomer("C1", 111222);
            s.DoBuy(5000);
            var c1 = s.DoBuy(100);
            Assert.AreEqual(95, c1);
            s.DoBuy(9500);
            c1 = s.DoBuy(100);
            Assert.AreEqual(90, c1);
            s.DoBuy(20000);
            c1 = s.DoBuy(100);
            Assert.AreEqual(85, c1);
            s.Logout();
        }

        [TestMethod]
        public void TestCheerfulCard()
        {
            Shop s = new Shop();
            s.RegiserCustomer("C1", 111222);
            s.DoBuy(5000);
            var c1 = s.DoBuy(100);
            Assert.AreEqual(95, c1);
            s.GetCheefulCard();
            c1 = s.DoBuy(100);
            Assert.AreEqual(90, c1);
            s.Logout();
        }

        [TestMethod]
        public void TestCyclicCard()
        {
            Shop s = new Shop();
            s.RegiserCustomer("C1", 111222);
            s.RevokeCyclicDiscountCard();
            s.DoBuy(26000);
            var c = s.DoBuy(100);
            Assert.AreEqual(85, c);
            s.Logout();
        }

        [TestMethod]
        public void TestToString()
        {
            Shop s = new Shop();
            s.RegiserCustomer("C1", 111222);
            Assert.IsTrue(s.ToString() != "");
            s.Logout();
        }
    }
}
