﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ITShop
{
    class Program
    {
        static void Main(string[] args)
        {
            ITShop shop = new ITShop();
            string status;
            string[] msg = File.ReadAllLines("GreetingMessage.txt", System.Text.Encoding.Default);
            foreach (string s in msg)
            {
                Console.WriteLine(s);
            }
            Console.Write("\nChoose type of interface ( type 1 - interactive, or 2 - demonstration ):  ");
            string t = Console.ReadLine();
            int type;
            bool success = int.TryParse(t, out type) && ((type == 1) || (type == 2));
            while (!success)
            {
                Console.WriteLine("Try Again:");
                t = Console.ReadLine();
                success = int.TryParse(t, out type) && ((type == 1) || (type == 2));
            }
            string command = "";
            switch (type)
            {
                case 1:
                    Console.Write("Enter command:  ");
                    command = Console.ReadLine().ToLower();
                    while (command != shop.EXIT)
                    {
                        if (shop.COMMANDS.Contains(command))
                        {
                            shop.DoAction(command, true);
                            Console.Write("Enter command:  ");
                        }
                        else
                            Console.WriteLine("Unknown commnd, try again");
                        command = Console.ReadLine();
                    }
                    status = shop.shop.ToString();
                    Console.WriteLine("Status of System:");
                    Console.Write(status);
                    break;
                case 2:
                    string fromRead, pathLog;
                    if (args.Length == 2)
                    {
                        fromRead = args[0];
                        pathLog = args[1];
                    }
                    else
                    {
                        Console.WriteLine("Enter path of file from where commands will be read and path of log file");
                        fromRead = Console.ReadLine();
                        pathLog = Console.ReadLine();
                    }
                    string[] lines = File.ReadAllLines(fromRead);
                    foreach (string s in lines)
                    {
                        string[] cmds = s.Split(' ');
                        switch (cmds.Length)
                        {
                            case 1:
                                shop.DoAction(cmds[0], false, pathLog);
                                break;
                            case 2:
                                shop.DoAction(cmds[0], false, pathLog, cmds[1]);
                                break;
                            case 3:
                                shop.DoAction(cmds[0], false, pathLog, cmds[1], cmds[2]);
                                break;
                        }
                    }
                    status = shop.shop.ToString();
                    StreamWriter sw = new StreamWriter(pathLog, true);
                    sw.WriteLine("\n  Status of System:");
                    sw.Write(status);
                    sw.Close();
                    break;
            }

            Console.ReadLine();
        }
    }
}
