﻿using System;
using System.IO;
using System.Collections.Generic;
using ITShopСore;

namespace ITShop
{
    class ITShop
    {
        public ITShop()
        { 
            COMMANDS = new List<string> {"register","authorize", "buy", "getchdc", "revokecydc", "logout"};
            EXIT = "exit";
            shop = new ITShopCore.Shop();
        }

        public string EXIT;
        public List<string> COMMANDS;
        public ITShopCore.Shop shop;

        // Проверка корректности введенных данных. Если не правильно введен номер паспорта, то вернется false
        private bool TryRegister(string name, string passnum)
        {
            int pn = 0;
            // если данные введены правильно
            if (int.TryParse(passnum, out pn))
            {
                try
                {
                    shop.RegiserCustomer(name, pn);
                    return true;
                }
                catch (System.ArgumentException e)
                { return false; }
            }
            else
                return false;
        }

        private bool TryAuthorize(string passnum)
        {
            int pn = 0;
            // если данные введены правильно
            if (int.TryParse(passnum, out pn) && pn >= 100000 && pn <=999999)
            {
                try
                {
                    shop.AuthorizeCustomer(pn);
                    return true;
                }
                catch (System.ArgumentException e)
                {
                    return false;
                }
            }
            else
                return false;
        }

        // Успешный исход: 1 - покупка совершена успешно,
        // Неудачные исходы: -1 - неверная стоимость, -2 - введена странная информация, 0 - пользователь не авторизован
        private int TryToBuy(string cost, out double q)
        {
            int c;
            q = 0;
            if (int.TryParse(cost, out c))
            {
                try
                {
                    q = shop.DoBuy(c);
                    return 1;
                }
                catch (System.ArgumentException e)
                {
                    return -1;
                }
                catch (System.NullReferenceException e)
                {
                    return 0;
                }
            }
            else
                return -2;
        }

        private bool TryGetCHeerfulCard()
        {
            try
            {
                shop.GetCheefulCard();
                return true;
            }
            catch (System.NullReferenceException e)
            {
                return false;
            }
        }

        private bool TryRevokeCyclicCard()
        {
            try
            {
                shop.RevokeCyclicDiscountCard();
                return true;
            }
            catch (System.NullReferenceException e)
            {
                return false;
            }
        }

        
        // Выполнятель команд
        public void DoAction(string cmd, bool interactive, string pathLog = "", params string[] args)
        {
            string passnum, name, cost;
            int codeOfMistakeInBuying;
            cmd = cmd.ToLower();
            // Удачна ли попытка выполнить команду?
            bool tryToDoSuccess = false;
            switch (cmd)
            {
                case "register":
                    if (interactive)
                    {
                        Console.Write("Enter name:  ");
                        name = Console.ReadLine();
                        Console.Write("Enter passport number ( 6 digits, from 100000 to 999999:  ");
                        passnum = Console.ReadLine();
                        tryToDoSuccess = TryRegister(name, passnum);
                        if (tryToDoSuccess)
                            Console.WriteLine("-- Register complete");
                        else
                             Console.WriteLine( "Wrong Passport Nubmer (enter 6 digits, i.e. 123456)");
                    }
                    else
                    {
                        name = args[0];
                        passnum = args[1];
                        tryToDoSuccess = TryRegister(name,passnum);
                        StreamWriter sw = new StreamWriter(pathLog, true);
                        if (tryToDoSuccess)
                            sw.WriteLine(string.Format("Register complete:  Name - {0}, Passport Number - {1}", name, passnum));
                        else
                            sw.WriteLine(string.Format("Register mistake: Wrong Passport number:  Name - {0}, Passport Number - {1}", name, passnum));
                        sw.Close();
                    }             
                    break;
                case "authorize":
                    if (interactive)
                    {
                        Console.Write("Enter passport number ( 6 digits, begun from 100000 to 999999:  ");
                        passnum = Console.ReadLine();
                        tryToDoSuccess = TryAuthorize(passnum);
                        if (tryToDoSuccess)
                            Console.WriteLine("-- Authorization complete: passport number - "+passnum);
                        else
                            Console.WriteLine("Wrong Passport Nubmer(There is no this customer in System or you make a mistake).");
                    }
                    else
                    {
                        passnum = args[0];
                        tryToDoSuccess = TryAuthorize(passnum);
                        StreamWriter sw = new StreamWriter(pathLog, true);
                        if (tryToDoSuccess)
                            sw.WriteLine(string.Format("Authorization complete: Passport Number - {0}",passnum));
                        else
                            sw.WriteLine(string.Format("Authorization mistake: Wrong Passport number: Passport Number - {0}", passnum));
                        sw.Close();
                    }
                    break;
                case "buy":
                    if (interactive)
                    {
                        Console.Write("Enter cost:  ");
                        cost = Console.ReadLine();
                        double finalCost = 0;
                        codeOfMistakeInBuying = TryToBuy(cost,out finalCost);
                        switch (codeOfMistakeInBuying)
                        {
                            case 0:
                                Console.WriteLine("Customer should be authorized in System");
                                break;
                            case -1:
                                Console.WriteLine("Cost cann't be minus");
                                break;
                            case -2:
                                Console.WriteLine("Mistake in writing cost");
                                break;
                            case 1:
                                Console.WriteLine("-- Buy complete: final cost - "+finalCost);
                                break;
                        }
                    }
                    else
                    {
                        cost = args[0];
                        double finalCost = 0;
                        codeOfMistakeInBuying = TryToBuy(cost, out finalCost);
                        StreamWriter sw = new StreamWriter(pathLog, true);
                        switch (codeOfMistakeInBuying)
                        {
                            case 0:
                                sw.WriteLine("Buy mistake: customer should be authorized in System");
                                break;
                            case -1 -2:
                                sw.WriteLine("Buy mistake: wrong cost");
                                break;
                            case 1:
                                sw.WriteLine("Buy complete: cost - "+finalCost);
                                break;
                        }
                        sw.Close();
                    }
                    break;
                case "getchdc":
                    tryToDoSuccess = TryGetCHeerfulCard();
                    if (interactive)
                    {
                        if (tryToDoSuccess)
                            Console.WriteLine("-- Cheerful card is given");
                        else
                            Console.WriteLine("Customer should be authorized in System");
                    }
                    else
                    {
                        StreamWriter sw = new StreamWriter(pathLog, true);
                        if (tryToDoSuccess)
                            sw.WriteLine("getchbc complete");
                        else
                            sw.WriteLine("getchdc mistake: Customer should be authorized in System");
                        sw.Close();
                    }
                    break;
                case "revokecydc":
                    tryToDoSuccess = TryRevokeCyclicCard();
                    if (interactive)
                    {
                        if (tryToDoSuccess)
                            Console.WriteLine("-- Cyclic card is revoke.");
                        else
                            Console.WriteLine("Customer should be authorized in System");
                    }
                    else
                    {
                        StreamWriter sw = new StreamWriter(pathLog, true);
                        if (tryToDoSuccess)
                            sw.WriteLine("Revokecydc complete");
                        else
                            sw.WriteLine("revokecydc mistake: Customer should be authorized in System");
                        sw.Close();
                    }
                    break;
                case "logout":
                    shop.Logout();
                    if (interactive)
                        Console.WriteLine("-- Logout");
                    else
                    {
                        StreamWriter sw = new StreamWriter(pathLog, true);
                        sw.WriteLine("Logout");
                        sw.Close();
                    }
                    break;
                default:
                    Console.WriteLine("Unknown command, try again");
                    break;
            }
        }     
    }
}