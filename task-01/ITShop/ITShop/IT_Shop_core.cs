﻿using System;
using System.Collections.Generic;

namespace ITShopСore
{
    // Класс Персона. У каждой Персоны есть имя и номер паспорта(6 цифр, уникальные для каждого,
    // начинается с 1). Объект класса Персона получает имя и номер при создании
    // и в дальнейшем они не меняются. Персона может сообщить свои данные, для этого есть геттеры.
    class Person
    {
        string name;
        int passportNumber;

        public Person(string Name, int passNum)
        {
            name = Name;
            if ((passNum > 999999) || (passNum < 100000))
                throw new System.ArgumentException("Passport Number cann't be less 100000 or more 999999", "passNum");
            passportNumber = passNum;
        }

        public int PasspornNumber
        {
            get { return passportNumber; }
        }

        public string Name
        {
            get { return name; }
        }

        virtual public string ToString()
        {
            return string.Format("Name: {0}; Passport number: {1}", name, passportNumber);
        }
    }

    // Класс покупатель, наследник класса Персона. Отличается тем, что может иметь скидочные
    // карты, совершать покупки.
    class Customer : Person
    {
        // Была ли у покупателя квантовая карта ( если была, то другая уже не выдается )
        public bool HadQuantiumCard;

        public Customer(string Name, int passNum) : base ( Name, passNum)
        {
            HadQuantiumCard = false;
        }

        // Покупатель умеет покупать. Данная функция принимает на вход стоимотсть покупки
        // и карту, на кот. совершается покупка. Стоимость прибавляется на карту.
        // Результат - цена с учетом скидки.
        public double Buy (int cost, DiscountCard card)
        {
            if (card != null)
            {
                card.AddSum(cost);
                return cost * card.factor;
            }
            else
                return cost;
        }
    }

//----------------------------------------

    // Базовый класс СкидочнаяКарта, от которого наследуются все виды скидочных карт.
    abstract public class DiscountCard
    {
        // Скидка в процентах ( число от 1 до 100 )
        public int discount;

        // Коэффициент, на который умножается стоимость покупки, чтобы получить стоимость со скидкой
        public double factor;

        // Накопленные деньги на карте
        public int accumulatedSum { get; set; }

        // Добавляет стоимость покупки cost к накопленной на карте сумме
        public virtual void AddSum(int cost)
        {
            accumulatedSum += cost;
        }

        public DiscountCard (int discount, int accSum = 0)
        {
            if ( (discount < 0) || (discount > 100) )
                throw new System.ArgumentException("Discount cann't be less 0 or more 100", "discount");
            this.discount = discount;
            accumulatedSum = accSum;
            factor = (100 - discount) * 0.01;
        }

        override public string ToString()
        {
            return string.Format("Discount: {0}; Accumulated sum: {1}",discount, accumulatedSum);
        }
    }

    // Ламповая скидочная карта выдаётся покупателю при накоплении 5000 суммарной
    // стоимости покупок. Эта карта даёт скидку 5%.
    public class TubeDiscountCard : DiscountCard
    {
        public TubeDiscountCard(int defaultSum, int disc = 5) : base (disc, defaultSum)
        {
            this.accumulatedSum = defaultSum;
        }

        public override string ToString()
        {
            var s = base.ToString();
            return "Type of card: Tube; "+s;
        }
       
    }

    // Транзисторная скидочная карта выдаётся при накоплении 12500
    // (может выдаваться после ламповой карты или сразу). Эта карта даёт скидку 10%.
    public class TransistorDiscountCard : DiscountCard
    {
        public TransistorDiscountCard(int defaultSum, int disc = 10) : base(disc, defaultSum)
        {
            this.accumulatedSum = defaultSum;
        }

        public override string ToString()
        {
            var s = base.ToString();
            return "Type of card: Transistor; " + s;
        }
    }

    // Интегральная скидочная карта выдаётся при накоплении 25000
    // (может выдаваться после ламповой карты, транзисторной карты или сразу). Эта карта даёт скидку 15%.
    public class IntegratedDiscountCard : DiscountCard
    {
        public IntegratedDiscountCard(int defaultSum, int disc = 15) : base(disc,defaultSum)
        {
            //base.discount = discount;
            //base.accumulatedSum = defaultSum;
        }

        public override string ToString()
        {
            var s = base.ToString();
            return "Type of card: Integrated; " + s;
        }
    }

    // Квантовая скидочная карта выдаётся покупателям в специальные «праздничные» дни
    // случайным образом. Она даёт скидку 20%. Если покупатель уже владел какой-то скидочной картой,
    // она (предыдущая карта) аннулируется, если только это уже не была квантовая карта (вторая не выдаётся).
    // Квантовая карта имеет ограниченный срок действия (например, полгода). После истечения срока
    // действия клиент получает взамен циклическую карту.
    public class QuantumDiscountCard : DiscountCard
    {
        // Срок действия - 155 дней
        const byte DaysOfExistCard = 155;
        public DateTime EndOfUsing;

        public QuantumDiscountCard(int disc = 20) : base(disc)
        {
            EndOfUsing = DateTime.Today.AddDays(DaysOfExistCard);
        }

        public override string ToString()
        {
            var s = base.ToString();
            return "Type of card: Quantium; " + s;
        }

    }

    // Циклическая скидочная карта является накопительной. В момент выдачи она
    // даёт скидку 5% (как ламповая) и содержит 5000 «накопительной» стоимости.
    // При накоплении 12500 она работает как транзисторная и даёт скидку в 10%. 
    // При накоплении 25000 — 15% (как интегральная). При накоплении 50000 сумма
    // накопления сбрасывается в 5000, и карта снова даёт скидку 5%.
    // Покупатель при желании может аннулировать эту карту и начать накопление заново.
    public class CyclicDiscountCard : DiscountCard
    {
        public CyclicDiscountCard(int disc = 5, int accumSum = 5000) : base(disc,accumSum)
        {
        }

        // Метод аннулирования карты. Счетчик "накопленной стоимости" сбрасывается на 0,
        // но скидка остается 5%
        public void revoke()
        {
            accumulatedSum = 0;
            discount = 5;
        }

        // Повышение накопленной стоимости. Если "накопл. стоим." получается
        // больше 50000, карта аннулируется. При достижении определенной суммы, повышается скидка.
        // (переопределение метода базового класса)
        public override void AddSum (int costOfBuying)
        {
            if (costOfBuying < 0)
                throw new System.ArgumentException("Cost of buying cann't be less 0", "costOfBuying");
            base.AddSum(costOfBuying);
            if (accumulatedSum >= 50000)
                revoke();
            else
                if (accumulatedSum >= 25000)
                    discount = 15;
                else
                    if (accumulatedSum >= 12500)
                        discount = 10;
        }

        public override string ToString()
        {
            var s = base.ToString();
            return "Type of card: Cyclic; " + s;
        }

    }

    // Весёлая скидочная карта — это единственная карта, которую можно
    // приобрести в магазине бесплатно. Она даёт скидку 10%, но работает только 10 дней в месяц,
    // причём покупатель не знает, какие это дни (магазин определяет эти дни
    // в начале каждого календарного месяца).
    public class CheerfulDiscountCard : DiscountCard
    {
        public CheerfulDiscountCard(int disc = 10) : base(disc) { }

        public override string ToString()
        {
            var s = base.ToString();
            return "Type of card: Cheerful; " + s;
        }
    }

//----------------------------------------
    interface IShop
    {
        void RegiserCustomer(string name, int passportNumber);
        void AuthorizeCustomer(int passportNumber);
        void Logout();
        double DoBuy(int cost);
        bool GetCheefulCard();
        bool RevokeCyclicDiscountCard();
    }

    // Класс Магазин. Основной класс
    public class Shop : IShop
    {
        // Словарь, в которому каждому номеру паспорта ставится в соответствие покупатель.
        // Таким образом, покупатель определяется номером паспорта, который у каждого уникальный
        private Dictionary<int,Customer> customers;

        // Каждому покупателю ставится в соответствие список его дисконтных карт.
        private Dictionary<Customer, List<DiscountCard>> cards;

        // Суммарная стоимость покупок, сделанных покупателем (потребуется для того,
        // чтобы выдать транзисторную карту, затем данный элемент словаря удаляется )
        private Dictionary<Customer, int> accumSums;

        // Генератор случайных чисел для выдачи квантовых карт
        Random random;

        // Текущий покупатель, то есть тот, кто совершает покупку в настоящий момент
        private Customer currentCustomer;

        public Shop()
        {
            customers = new Dictionary<int, Customer>();
            currentCustomer = null;
            random = new Random();
            accumSums = new Dictionary<Customer, int>();
            cards = new Dictionary<Customer, List<DiscountCard>>();
        }

        // Регистрация нового покупателя
        public void RegiserCustomer(string name, int passportNumber)
        {
            customers.Add(passportNumber, new Customer(name,passportNumber));
            AuthorizeCustomer(passportNumber);
            accumSums.Add(currentCustomer,0);
            cards[currentCustomer] = new List<DiscountCard>();
        }

        // Авторизация существующего покупатея
        public void AuthorizeCustomer(int passportNumber)
        {
            currentCustomer = customers[passportNumber];
        }

        // Завершение сеанса работы.
        public void Logout()
        {
            currentCustomer = null;
        }

        // Текущий покупатель совершает покупку ценой cost. При этом может улучшится карта (ламп - транз, транз-инт)
        // или выдаться карта, если ее не было и накопленная сумма позволяет.
        // (сумма накопления на карте увеличивается в методе Buy покупателя)
        public double DoBuy ( int cost )
        {
            if (cost <= 0)
                throw new System.ArgumentException("Cost cann't be less or equal 0");
            if (currentCustomer == null)
                throw new System.NullReferenceException("Customer should be authourized in system");
            List<DiscountCard> _cards = cards[currentCustomer];
            // Если у покупателя есть или была квантовая карта
            if (currentCustomer.HadQuantiumCard)
            {
                // Найти ее в списке карт ( после квантовой покупатель мог взять веселую карту
                var a = _cards.Find( (c) => {return c.GetType() == (new QuantumDiscountCard()).GetType();});
                // Если она уже не действительна, то удалить ее и дать вместо нее циклическую
                if ( (a as QuantumDiscountCard).EndOfUsing < DateTime.Today)
                {
                    _cards.Remove(a);
                    _cards.Add(new CyclicDiscountCard());
                }
            }
            // У покупателя могут быть карты. Если их нет, вернется null           
            var dc = ChooseCardWithTheLargestDiscount();
            // если покупатель еще не накопил нужную сумму для получения ламп, транз или интегр карт ( минимум - 5000 )
            if (accumSums.ContainsKey(currentCustomer))
            {
                accumSums[currentCustomer] += cost;
                int sum = accumSums[currentCustomer];
                // Можно ли вообще выдать карту
                if (sum >= 5000)
                {
                    // Можно ли выдать интегральную карту
                    if (sum >= 25000)
                    {
                        var c = new IntegratedDiscountCard(sum);
                        _cards.Add(c);
                    }
                    else
                        // Можно ли выдать транзисторную карту
                        if (sum >= 12500)
                            _cards.Add(new TransistorDiscountCard(sum));
                        else
                            _cards.Add(new TubeDiscountCard(sum));
                    accumSums.Remove(currentCustomer);
                }                
            }
            // Попытка улучшить карту
            if  ( dc != null )
                cards[currentCustomer][0] = LevelUpCard(dc);
            // ПРи покупке покупателю может быть выдана квантовая карта...если ему повезет
            if (!currentCustomer.HadQuantiumCard && (random.Next(0, 10) == 7))
            {
                _cards.Add(new QuantumDiscountCard());
                currentCustomer.HadQuantiumCard = true;
            }
            // Если карта выдалась во время данной покупки, то она не учитывается и
            // если у покупателя до этого не было карт, то он купит товар без скидки
            return currentCustomer.Buy(cost, dc);
        }

        // Выбрать карту текущего покупателя с наибольшей скидкой
        // Если у покупателя нет карт, то возвращает null
        private DiscountCard ChooseCardWithTheLargestDiscount()
        {
            if (cards[currentCustomer].Count == 0)
                return null;
            else
            {
                cards[currentCustomer].Sort(LargestDiscount);
                return cards[currentCustomer][0];
            }
        }

        private int LargestDiscount(DiscountCard a, DiscountCard b)
        {
            if (a.discount < b.discount)
                return 1;
            else
                if (a.discount > b.discount)
                    return -1;
                else return 0;
        }

        // Повышение карты ( ламповая -> транзисторная или транз -> интегральная или ламп -> инт ).
        private DiscountCard LevelUpCard(DiscountCard dc)
        {
            if ((dc is TubeDiscountCard) && (dc.accumulatedSum >= 12500))
                return new TransistorDiscountCard(dc.accumulatedSum);
            else
                if (((dc is TransistorDiscountCard) && (dc.accumulatedSum >= 25000)) ||
                    ((dc is TubeDiscountCard) && (dc.accumulatedSum >= 25000)))
                {
                    return new IntegratedDiscountCard(dc.accumulatedSum);
                }
                else
                    return dc;
        }

        // Приобрести Веселую карту
        public bool GetCheefulCard()
        {
            if (currentCustomer == null)
                throw new System.NullReferenceException("Customer should be authourized in system");
            CheerfulDiscountCard cdc = new CheerfulDiscountCard();
            Predicate<DiscountCard> pred = (dc) => { return dc.GetType() == cdc.GetType(); };
            // Индекс карты ( -1, если нет ее )
            int ind = cards[currentCustomer].IndexOf(cards[currentCustomer].Find(pred));
            // Если нет веселой карты
            if (ind > -1)
                // Не добавилась
                return false;
            else
            {
                // Добавить
                cards[currentCustomer].Add(cdc);
                // Сказать, что добавилась
                return true;
            }
        }

        // Аннулирование циклический карты. Если такая карта не находится в списке карт покупателя, то
        // может ее получить, при условии что у него уже есть ламповая, транзсторная или интегральная карта
        // ( то есть он уже купил товаров в магазине на сумму >= 5000).
        public bool RevokeCyclicDiscountCard()
        {
            if (currentCustomer == null)
                throw new System.NullReferenceException("Customer should be authourized in system");
            CyclicDiscountCard cdc = new CyclicDiscountCard();
            Predicate<DiscountCard> pred = (dc) => { return dc.GetType() == cdc.GetType(); };
            // Индекс цикл. карты ( -1, если нет ее )
            int ind = cards[currentCustomer].IndexOf(cards[currentCustomer].Find(pred));
            // Если есть цикл. карта
            if (ind > -1)
            {
                // Аннулировалась
                return true;
                var dc = cards[currentCustomer][ind];
                (dc as CyclicDiscountCard).revoke();
            }
            else
            {
                // Не аннулировалась
                return false;
                // Если покупатель НЕ в списке тех, кто купил на сумму меньше, чем 5000
                if (!accumSums.ContainsKey(currentCustomer))
                    cards[currentCustomer].Add(new CyclicDiscountCard());
            }
        }

        // Функция выдачи квантовой карты. Все остальные карты уничтожаются, покупатель снова
        // находится в списке тех, кто копит 5000 на первую ламповую карту. Взамен всего этого
        // выдается квантовая карта
        private void GiveQuantiumCard()
        {
            cards[currentCustomer].Clear();
            cards[currentCustomer].Add ( new QuantumDiscountCard () );
            accumSums[currentCustomer] = 0;
        }

        override public string ToString()
        {
            string info ="";
            foreach (KeyValuePair<int, Customer> c in this.customers)
            {
                info += "\n" + c.Value.ToString();
                // Если покупатель имеет карты
                if (cards[c.Value].Count != 0)
                {
                    List<DiscountCard> cs = cards[c.Value];
                    foreach (DiscountCard dc in cs)
                    {
                        info += "\n  " + dc.ToString();
                    }
                }
                // В противном случае (если у покупателя нет карт), то выводится накопленная сумма
                else
                {
                    info += "  Accumulated Sum: " + accumSums[c.Value];
                }               
            }
            return info;
        }
    }
}