﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using ITShopСore.Cards;

namespace ITShopCore
{
        interface IShop
    {
        void RegiserCustomer(string name, int passportNumber);
        void AuthorizeCustomer(int passportNumber);
        void Logout();
        double DoBuy(int cost);
        bool GetCheefulCard();
        bool RevokeCyclicDiscountCard();
    }

    // Класс Магазин. Основной класс
    public class Shop : IShop
    {
        private const int SUM_TO_GET_TUBE_DC = 5000;
        private const int SUM_TO_GET_INTEGRATED_DC = 25000;
        private const int SUM_TO_GET_TRANSISTOR_DC = 12500;
        // Словарь, в которому каждому номеру паспорта ставится в соответствие покупатель.
        // Таким образом, покупатель определяется номером паспорта, который у каждого уникальный
        private Dictionary<int,Customer> customers;

        // Каждому покупателю ставится в соответствие список его дисконтных карт.
        private Dictionary<Customer, List<DiscountCard>> cards;

        // Суммарная стоимость покупок, сделанных покупателем (потребуется для того,
        // чтобы выдать транзисторную карту, затем данный элемент словаря удаляется )
        private Dictionary<Customer, int> accumSums;

        // Генератор случайных чисел для выдачи квантовых карт
        Random random;

        // Текущий покупатель, то есть тот, кто совершает покупку в настоящий момент
        private Customer currentCustomer;

        public Shop()
        {
            customers = new Dictionary<int, Customer>();
            currentCustomer = null;
            random = new Random();
            accumSums = new Dictionary<Customer, int>();
            cards = new Dictionary<Customer, List<DiscountCard>>();
        }

        // Регистрация нового покупателя
        public void RegiserCustomer(string name, int passportNumber)
        {
            customers.Add(passportNumber, new Customer(name,passportNumber));
            AuthorizeCustomer(passportNumber);
            accumSums.Add(currentCustomer,0);
            cards[currentCustomer] = new List<DiscountCard>();
        }

        // Авторизация существующего покупатея
        public void AuthorizeCustomer(int passportNumber)
        {
            currentCustomer = customers[passportNumber];
        }

        // Завершение сеанса работы.
        public void Logout()
        {
            currentCustomer = null;
        }

        // Текущий покупатель совершает покупку ценой cost. При этом может улучшится карта (ламп - транз, транз-инт)
        // или выдаться карта, если ее не было и накопленная сумма позволяет.
        // (сумма накопления на карте увеличивается в методе Buy покупателя)
        public double DoBuy ( int cost )
        {
            Debug.Assert(cost>0,"Cost cann't be less or equal 0");
            Debug.Assert(currentCustomer != null, "Customer should be authourized in system");
            if (cost <= 0)
                throw new System.ArgumentOutOfRangeException ("Cost cann't be less or equal 0");
            if (currentCustomer == null)
                throw new System.NullReferenceException ("Customer should be authourized in system");
            List<DiscountCard> _cards = cards[currentCustomer];
            // Если у покупателя есть или была квантовая карта
            if (currentCustomer.HadQuantiumCard)
            {
                // Найти ее в списке карт ( после квантовой покупатель мог взять веселую карту
                var a = _cards.Find( (c) => {return c.GetType() == (new QuantumDiscountCard()).GetType();});
                // Если она уже не действительна, то удалить ее и дать вместо нее циклическую
                if ( (a as QuantumDiscountCard).EndOfUsing < DateTime.Today)
                {
                    _cards.Remove(a);
                    _cards.Add(new CyclicDiscountCard());
                }
            }
            // У покупателя могут быть карты. Если их нет, вернется null           
            var dc = ChooseCardWithTheLargestDiscount();
            // если покупатель еще не накопил нужную сумму для получения ламп, транз или интегр карт ( минимум - 5000 )
            if (accumSums.ContainsKey(currentCustomer))
            {
                accumSums[currentCustomer] += cost;
                int sum = accumSums[currentCustomer];
                // Можно ли вообще выдать карту
                if (sum >= SUM_TO_GET_TUBE_DC)
                {
                    // Можно ли выдать интегральную карту
                    if (sum >= SUM_TO_GET_INTEGRATED_DC)
                    {
                        var c = new IntegratedDiscountCard(sum);
                        _cards.Add(c);
                    }
                    else
                        // Можно ли выдать транзисторную карту
                        if (sum >= SUM_TO_GET_TRANSISTOR_DC)
                            _cards.Add(new TransistorDiscountCard(sum));
                        else
                            _cards.Add(new TubeDiscountCard(sum));
                    accumSums.Remove(currentCustomer);
                }                
            }
            // Попытка улучшить карту
            if  ( dc != null && dc is DiscountCardAccumSum )
                dc = cards[currentCustomer][0] = LevelUpCard(dc as DiscountCardAccumSum);
            // ПРи покупке покупателю может быть выдана квантовая карта...если ему повезет
            if (!currentCustomer.HadQuantiumCard && (random.Next(0, 10) == 11))
            {
                _cards.Add(new QuantumDiscountCard());
                currentCustomer.HadQuantiumCard = true;
            }
            // Если карта выдалась во время данной покупки, то она не учитывается и
            // если у покупателя до этого не было карт, то он купит товар без скидки
            return currentCustomer.Buy(cost, dc);
        }

        // Выбрать карту текущего покупателя с наибольшей скидкой
        // Если у покупателя нет карт, то возвращает null
        private DiscountCard ChooseCardWithTheLargestDiscount()
        {
            if (cards[currentCustomer].Count == 0)
                return null;
            else
            {
                cards[currentCustomer].Sort(LargestDiscount);
                return cards[currentCustomer][0];
            }
        }

        private int LargestDiscount(DiscountCard a, DiscountCard b)
        {
            if (a.discount < b.discount)
                return 1;
            else
                if (a.discount > b.discount)
                    return -1;
                else return 0;
        }

        // Повышение карты ( ламповая -> транзисторная или транз -> интегральная или ламп -> инт ).
        private DiscountCardAccumSum LevelUpCard(DiscountCardAccumSum dc)
        {
            if ((dc is TubeDiscountCard) && (dc.accumulatedSum >= SUM_TO_GET_TRANSISTOR_DC))
                return new TransistorDiscountCard(dc.accumulatedSum);
            else
                if (((dc is TransistorDiscountCard) && (dc.accumulatedSum >= SUM_TO_GET_INTEGRATED_DC)) ||
                    ((dc is TubeDiscountCard) && (dc.accumulatedSum >= SUM_TO_GET_INTEGRATED_DC)))
                {
                    return new IntegratedDiscountCard(dc.accumulatedSum);
                }
                else
                    return dc;
        }

        // Приобрести Веселую карту
        public bool GetCheefulCard()
        {
            Debug.Assert(currentCustomer != null, "Customer should be authourized in system");
            if (currentCustomer == null)
                throw new System.NullReferenceException("Customer should be authourized in system");
            CheerfulDiscountCard cdc = new CheerfulDiscountCard();
            Predicate<DiscountCard> pred = (dc) => { return dc.GetType() == cdc.GetType(); };
            // Индекс карты ( -1, если нет ее )
            int ind = cards[currentCustomer].IndexOf(cards[currentCustomer].Find(pred));
            // Если нет веселой карты
            if (ind > -1)
                // Не добавилась
                return false;
            else
            {
                // Добавить
                cards[currentCustomer].Add(cdc);
                // Сказать, что добавилась
                return true;
            }
        }

        // Аннулирование циклический карты. Если такая карта не находится в списке карт покупателя, то
        // может ее получить, при условии что у него уже есть ламповая, транзсторная или интегральная карта
        // ( то есть он уже купил товаров в магазине на сумму >= 5000).
        public bool RevokeCyclicDiscountCard()
        {
            Debug.Assert(currentCustomer != null, "Customer should be authourized in system");
            if (currentCustomer == null)
                throw new System.NullReferenceException("Customer should be authourized in system");
            CyclicDiscountCard cdc = new CyclicDiscountCard();
            Predicate<DiscountCard> pred = (dc) => { return dc.GetType() == cdc.GetType(); };
            // Индекс цикл. карты ( -1, если нет ее )
            int ind = cards[currentCustomer].IndexOf(cards[currentCustomer].Find(pred));
            // Если есть цикл. карта
            if (ind > -1)
            {
                // Аннулировалась
                return true;
                var dc = cards[currentCustomer][ind];
                (dc as CyclicDiscountCard).revoke();
            }
            else
            {
                // Если покупатель НЕ в списке тех, кто купил на сумму меньше, чем 5000
                if (!accumSums.ContainsKey(currentCustomer))
                    cards[currentCustomer].Add(new CyclicDiscountCard());
                // Не аннулировалась
                return false;
            }
        }

        // Функция выдачи квантовой карты. Все остальные карты уничтожаются, покупатель снова
        // находится в списке тех, кто копит 5000 на первую ламповую карту. Взамен всего этого
        // выдается квантовая карта
        private void GiveQuantiumCard()
        {
            cards[currentCustomer].Clear();
            cards[currentCustomer].Add ( new QuantumDiscountCard () );
            accumSums[currentCustomer] = 0;
        }

        override public string ToString()
        {
            string info ="";
            foreach (KeyValuePair<int, Customer> c in this.customers)
            {
                info += "\n" + c.Value.ToString();
                // Если покупатель имеет карты
                if (cards[c.Value].Count != 0)
                {
                    List<DiscountCard> cs = cards[c.Value];
                    foreach (DiscountCard dc in cs)
                    {
                        info += "\n  " + dc.ToString();
                    }
                }
                // В противном случае (если у покупателя нет карт), то выводится накопленная сумма
                else
                {
                    info += "  Accumulated Sum: " + accumSums[c.Value];
                }               
            }
            return info;
        }
    }
}
