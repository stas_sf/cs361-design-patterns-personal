﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ITShopCore
{
    // Класс Персона. У каждой Персоны есть имя и номер паспорта(6 цифр, уникальные для каждого,
    // начинается с 1). Объект класса Персона получает имя и номер при создании
    // и в дальнейшем они не меняются. Персона может сообщить свои данные, для этого есть геттеры.
    class Person
    {
        string name;
        int passportNumber;

        public Person(string Name, int passNum)
        {
            name = Name;
            if ((passNum > 999999) && (passNum >= 100000))
                throw new System.ArgumentOutOfRangeException("passNum","Passport Number cann't be less 100000 or more 999999");
            passportNumber = passNum;
        }

        public int PasspornNumber
        {
            get { return passportNumber; }
        }

        public string Name
        {
            get { return name; }
        }

        virtual public string ToString()
        {
            return string.Format("Name: {0}; Passport number: {1}", name, passportNumber);
        }
    }
}
