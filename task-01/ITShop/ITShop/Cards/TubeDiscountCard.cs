﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITShopСore.Cards
{
    // Ламповая скидочная карта выдаётся покупателю при накоплении 5000 суммарной
    // стоимости покупок. Эта карта даёт скидку 5%.
    class TubeDiscountCard : DiscountCardAccumSum
    {
        public TubeDiscountCard(int defaultSum, int disc = 5)
            : base(disc, defaultSum)
        {
            this.accumulatedSum = defaultSum;
        }

        public override string ToString()
        {
            var s = base.ToString();
            return "Type of card: Tube; " + s;
        }
    }
}