﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITShopСore.Cards
{
    // Интегральная скидочная карта выдаётся при накоплении 25000
    // (может выдаваться после ламповой карты, транзисторной карты или сразу). Эта карта даёт скидку 15%.
    class IntegratedDiscountCard : DiscountCardAccumSum
    {
        public IntegratedDiscountCard(int defaultSum, int disc = 15) : base(disc, defaultSum)
        {
        }

        public override string ToString()
        {
            var s = base.ToString();
            return "Type of card: Integrated; " + s;
        }
    }
}
