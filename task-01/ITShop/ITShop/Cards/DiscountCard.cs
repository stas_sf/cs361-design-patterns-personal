﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ITShopСore.Cards
{
    // Базовый класс СкидочнаяКарта, от которого наследуются все виды скидочных карт.
    abstract public class DiscountCard
    {
        // Скидка в процентах ( число от 1 до 100 )
        public int discount;

        // Коэффициент, на который умножается стоимость покупки, чтобы получить стоимость со скидкой
        public double factor;

        public DiscountCard(int discount)
        {
            Debug.Assert((discount >= 0) || (discount <= 100), "Discount cann't be less 0 or more 100");
            if ((discount < 0) || (discount > 100))
                throw new System.ArgumentException("Discount cann't be less 0 or more 100");
            this.discount = discount;
            factor = (100 - discount) * 0.01;
        }
        public virtual void AddSum(int cost) { }

        override public string ToString()
        {
            return string.Format("Discount: {0}; Accumulated sum: {1}", discount);
        }
    }
}
