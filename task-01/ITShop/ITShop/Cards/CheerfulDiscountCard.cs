﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITShopСore.Cards
{
    // Весёлая скидочная карта — это единственная карта, которую можно
    // приобрести в магазине бесплатно. Она даёт скидку 10%, но работает только 10 дней в месяц,
    // причём покупатель не знает, какие это дни (магазин определяет эти дни
    // в начале каждого календарного месяца).
    class CheerfulDiscountCard:DiscountCard
    {
        public CheerfulDiscountCard(int disc = 10) : base(disc) { }

        public override string ToString()
        {
            var s = base.ToString();
            return "Type of card: Cheerful; " + s;
        }
    }
}
