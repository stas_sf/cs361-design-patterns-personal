﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITShopСore.Cards
{
    // Квантовая скидочная карта выдаётся покупателям в специальные «праздничные» дни
    // случайным образом. Она даёт скидку 20%. Если покупатель уже владел какой-то скидочной картой,
    // она (предыдущая карта) аннулируется, если только это уже не была квантовая карта (вторая не выдаётся).
    // Квантовая карта имеет ограниченный срок действия (например, полгода). После истечения срока
    // действия клиент получает взамен циклическую карту.
    class QuantumDiscountCard : DiscountCardAccumSum
    {
        // Срок действия - 155 дней
        const byte DaysOfExistCard = 155;
        public DateTime EndOfUsing;

        public QuantumDiscountCard(int disc = 20)
            : base(disc)
        {
            EndOfUsing = DateTime.Today.AddDays(DaysOfExistCard);
        }

        public override string ToString()
        {
            var s = base.ToString();
            return "Type of card: Quantium; " + s;
        }
    }
}
