﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ITShopСore.Cards
{
    abstract class DiscountCardAccumSum : DiscountCard
    {
        // Накопленные деньги на карте
        public int accumulatedSum { get; set; }

        // Добавляет стоимость покупки cost к накопленной на карте сумме
        public virtual void AddSum(int cost)
        {
            accumulatedSum += cost;
        }

        public DiscountCardAccumSum(int discount, int accSum = 0) : base (discount)
        {
            //if ((discount < 0) || (discount > 100))
            //    throw new System.ArgumentException("Discount cann't be less 0 or more 100", "discount");
            Debug.Assert(accSum >= 0, "Accumulated Sum can't be less 0");
            this.discount = discount;
            accumulatedSum = accSum;
            factor = (100 - discount) * 0.01;
        }

        override public string ToString()
        {
            return string.Format("Discount: {0}; Accumulated sum: {1}", discount, accumulatedSum);
        }
    }
}
