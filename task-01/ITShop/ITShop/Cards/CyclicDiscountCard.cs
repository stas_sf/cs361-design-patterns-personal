﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITShopСore.Cards
{
    // Циклическая скидочная карта является накопительной. В момент выдачи она
    // даёт скидку 5% (как ламповая) и содержит 5000 «накопительной» стоимости.
    // При накоплении 12500 она работает как транзисторная и даёт скидку в 10%. 
    // При накоплении 25000 — 15% (как интегральная). При накоплении 50000 сумма
    // накопления сбрасывается в 5000, и карта снова даёт скидку 5%.
    // Покупатель при желании может аннулировать эту карту и начать накопление заново.
    class CyclicDiscountCard : DiscountCardAccumSum
    {
        private const int SUM_TO_REVOKE = 50000;
        private const int SUM_TO_DISCOUNT15 = 25000;
        private const int SUM_TO_DISCOUNT10 = 12500;
        public CyclicDiscountCard(int disc = 5, int accumSum = 5000)
            : base(disc, accumSum)
        {
        }

        // Метод аннулирования карты. Счетчик "накопленной стоимости" сбрасывается на 0,
        // но скидка остается 5%
        public void revoke()
        {
            accumulatedSum = 0;
            discount = 5;
        }

        // Повышение накопленной стоимости. Если "накопл. стоим." получается
        // больше 50000, карта аннулируется. При достижении определенной суммы, повышается скидка.
        // (переопределение метода базового класса)
        public override void AddSum(int costOfBuying)
        {
            if (costOfBuying < 0)
                throw new System.ArgumentException("Cost of buying cann't be less 0", "costOfBuying");
            base.AddSum(costOfBuying);
            if (accumulatedSum >= SUM_TO_REVOKE)
                revoke();
            else
                if (accumulatedSum >= SUM_TO_DISCOUNT15)
                    discount = 15;
                else
                    if (accumulatedSum >= SUM_TO_DISCOUNT10)
                        discount = 10;
        }

        public override string ToString()
        {
            var s = base.ToString();
            return "Type of card: Cyclic; " + s;
        }
    }
}
