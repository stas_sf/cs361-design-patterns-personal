﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITShopСore.Cards
{
    // Транзисторная скидочная карта выдаётся при накоплении 12500
    // (может выдаваться после ламповой карты или сразу). Эта карта даёт скидку 10%.
    class TransistorDiscountCard : DiscountCardAccumSum
    {
        public TransistorDiscountCard(int defaultSum, int disc = 10)
            : base(disc, defaultSum)
        {
            this.accumulatedSum = defaultSum;
        }

        public override string ToString()
        {
            var s = base.ToString();
            return "Type of card: Transistor; " + s;
        }
    }
}
