﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ITShopСore.Cards;

namespace ITShopCore
{
    // Класс покупатель, наследник класса Персона. Отличается тем, что может иметь скидочные
    // карты, совершать покупки.
    class Customer : Person
    {
        // Была ли у покупателя квантовая карта ( если была, то другая уже не выдается )
        public bool HadQuantiumCard;

        public Customer(string Name, int passNum)
            : base(Name, passNum)
        {
            HadQuantiumCard = false;
        }

        // Покупатель умеет покупать. Данная функция принимает на вход стоимотсть покупки
        // и карту, на кот. совершается покупка. Стоимость прибавляется на карту.
        // Результат - цена с учетом скидки.
        public double Buy(int cost, DiscountCard card)
        {
            if (card != null)
            {
                if (card is DiscountCardAccumSum)
                    (card as DiscountCardAccumSum).AddSum(cost);
                return cost * card.factor;
            }
            else
                return cost;
        }
    }
}
