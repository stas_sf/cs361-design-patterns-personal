﻿using System;
using System.IO;
using System.Collections.Generic;
using ITShopСore;

namespace ITShop
{
    class ITShop
    {
        public ITShop()
        { 
            COMMANDS = new List<string> {"register","authorize", "buy", "getchdc", "revokecydc", "logout"};
            EXIT = "exit";
            shop = new ITShopСore.Shop();
        }

        public string EXIT;
        public List<string> COMMANDS;
        public ITShopСore.Shop shop;

        // Проверка корректности введенных данных. Если не правильно введен номер паспорта, то вернется false
        private bool TryRegister(string name, string passnum)
        {
            int pn = 0;
            // если данные введены правильно
            if (int.TryParse(passnum, out pn))
            {
                try
                {
                    shop.RegiserCustomer(name, pn);
                    return true;
                }
                catch (System.ArgumentException e)
                { return false; }
            }
            else
                return false;
        }

        private bool TryAuthorize(string passnum)
        {
            int pn = 0;
            // если данные введены правильно
            if (int.TryParse(passnum, out pn) && pn >= 100000)
            {
                try
                {
                    shop.AuthorizeCustomer(pn);
                    return true;
                }
                catch (System.ArgumentException e)
                {
                    return false;
                }
            }
            else
                return false;
        }

        // Успешный исход: 1 - покупка совершена успешно,
        // Неудачные исходы: -1 - неверная стоимость, -2 - введена странная информация, 0 - пользователь не авторизован
        private int TryToBuy(string cost, out double q)
        {
            int c;
            q = 0;
            if (int.TryParse(cost, out c))
            {
                try
                {
                    q = shop.DoBuy(c);
                    return 1;
                }
                catch (System.ArgumentException e)
                {
                    return -1;
                }
                catch (System.NullReferenceException e)
                {
                    return 0;
                }
            }
            else
                return -2;
        }

        private bool TryGetCHeerfulCard()
        {
            try
            {
                shop.GetCheefulCard();
                return true;
            }
            catch (System.NullReferenceException e)
            {
                return false;
            }
        }

        private bool TryRevokeCyclicCard()
        {
            try
            {
                shop.RevokeCyclicDiscountCard();
                return true;
            }
            catch (System.NullReferenceException e)
            {
                return false;
            }
        }

        
        // Выполнятель команд
        public void DoAction(string cmd, bool interactive, string pathLog = "", params string[] args)
        {
            string passnum, name, cost;
            int codeOfMistakeInBuying;
            // Удачна ли попытка выполнить команду?
            bool tryToDoSuccess = false;
            switch (cmd)
            {
                case "register":
                    if (interactive)
                    {
                        Console.WriteLine("Enter name:  ");
                        name = Console.ReadLine();
                        Console.WriteLine("Enter passport number ( 6 digits, begun from 100000 to 999999:  ");
                        passnum = Console.ReadLine();
                        tryToDoSuccess = TryRegister(name, passnum);
                        if (tryToDoSuccess)
                            Console.WriteLine("register complete");
                        else
                             Console.WriteLine( "Wrong Passport Nubmer.");
                    }
                    else
                    {
                        name = args[0];
                        passnum = args[1];
                        tryToDoSuccess = TryRegister(name,passnum);
                        StreamWriter sw = new StreamWriter(pathLog, true);
                        if (tryToDoSuccess)
                            sw.WriteLine(string.Format("Register complete:  Name - {0}, Passport Number - {1}", name, passnum));
                        else
                            sw.WriteLine(string.Format("Register mistake: Wrong Passport number:  Name - {0}, Passport Number - {1}", name, passnum));
                        sw.Close();
                    }             
                    break;
                case "authorize":
                    if (interactive)
                    {
                        Console.WriteLine("Enter passport number ( 6 digits, begun from 100000 to 999999:  ");
                        passnum = Console.ReadLine();
                        tryToDoSuccess = TryAuthorize(passnum);
                        if (tryToDoSuccess)
                            Console.WriteLine("Authorization complete: passport number - "+passnum);
                        else
                            Console.WriteLine("Wrong Passport Nubmer(There is no this customer in System or you make a mistake).");
                    }
                    else
                    {
                        passnum = args[0];
                        tryToDoSuccess = TryAuthorize(passnum);
                        StreamWriter sw = new StreamWriter(pathLog, true);
                        if (tryToDoSuccess)
                            sw.WriteLine(string.Format("Authorization complete: Passport Number - {0}",passnum));
                        else
                            sw.WriteLine(string.Format("Authorization mistake: Wrong Passport number: Passport Number - {0}", passnum));
                        sw.Close();
                    }
                    break;
                case "buy":
                    if (interactive)
                    {
                        Console.WriteLine("Enter cost:  ");
                        cost = Console.ReadLine();
                        double finalCost = 0;
                        codeOfMistakeInBuying = TryToBuy(cost,out finalCost);
                        switch (codeOfMistakeInBuying)
                        {
                            case 0:
                                Console.WriteLine("Customer should be authorized in System");
                                break;
                            case -1:
                                Console.WriteLine("Wrong cost");
                                break;
                            case -2:
                                Console.WriteLine("Mistake in writing cost");
                                break;
                            case 1:
                                Console.WriteLine("Buy complete: cost - "+finalCost);
                                break;
                        }
                    }
                    else
                    {
                        cost = args[0];
                        double finalCost = 0;
                        codeOfMistakeInBuying = TryToBuy(cost, out finalCost);
                        StreamWriter sw = new StreamWriter(pathLog, true);
                        switch (codeOfMistakeInBuying)
                        {
                            case 0:
                                sw.WriteLine("Buy mistake: customer should be authorized in System");
                                break;
                            case -1 -2:
                                sw.WriteLine("Buy mistake: wrong cost");
                                break;
                            case 1:
                                sw.WriteLine("Buy complete: cost - "+finalCost);
                                break;
                        }
                        sw.Close();
                    }
                    break;
                case "getchdc":
                    tryToDoSuccess = TryGetCHeerfulCard();
                    if (interactive)
                    {
                        if (tryToDoSuccess)
                            Console.WriteLine("getchbc complete");
                        else
                            Console.WriteLine("Customer should be authorized in System");
                    }
                    else
                    {
                        StreamWriter sw = new StreamWriter(pathLog, true);
                        if (tryToDoSuccess)
                            sw.WriteLine("getchbc complete");
                        else
                            sw.WriteLine("getchdc mistake: Customer should be authorized in System");
                        sw.Close();
                    }
                    break;
                case "revokecydc":
                    tryToDoSuccess = TryRevokeCyclicCard();
                    if (interactive)
                    {
                        if (tryToDoSuccess)
                            Console.WriteLine("revokecydc complete");
                        else
                            Console.WriteLine("Customer should be authorized in System");
                    }
                    else
                    {
                        StreamWriter sw = new StreamWriter(pathLog, true);
                        if (tryToDoSuccess)
                            sw.WriteLine("revokecydc complete");
                        else
                            sw.WriteLine("revokecydc mistake: Customer should be authorized in System");
                        sw.Close();
                    }
                    break;
                case "logout":
                    shop.Logout();
                    if (interactive)
                        Console.WriteLine("Logout");
                    else
                    {
                        StreamWriter sw = new StreamWriter(pathLog, true);
                        sw.WriteLine("Logout");
                        sw.Close();
                    }
                    break;
                default:
                    Console.WriteLine("Unknown command, try again");
                    break;
            }
        }     
    }

    class Program
    {
        static void Main(string[] args)
        {
            ITShop shop = new ITShop();
            string status;
            string[] msg = File.ReadAllLines("GreetingMessage.txt", System.Text.Encoding.Default);
            foreach (string s in msg)
            {
                Console.WriteLine(s);
            }
            Console.Write("\nChoose type of interface ( type 1 - interactive, or 2 - demonstration ):  ");
            string t = Console.ReadLine();
            int type;
            bool success = int.TryParse(t, out type) && ((type == 1) || (type == 2));
            while (!success)
            {               
                Console.WriteLine("Try Again:");
                t = Console.ReadLine();
                success = int.TryParse(t, out type) && ((type == 1) || (type == 2));
            }
            string command = "go";
            switch (type)
            {
                case 1:
                    command = Console.ReadLine();
                    while (command != shop.EXIT)
                    {
                        if (shop.COMMANDS.Contains(command))
                        {
                            shop.DoAction(command, true);
                        }
                        else
                            Console.WriteLine("Unknown commnd, try again");
                        command = Console.ReadLine();
                    }
                    status = shop.shop.ToString();
                    Console.WriteLine("Status of System:");
                    Console.Write(status);
                    break;
                case 2:
                    string fromRead,pathLog;
                    if ( args.Length == 2 )
                    {
                        fromRead = args[0];
                        pathLog = args[1];
                    }
                    else
                    {
                        Console.WriteLine("Enter path of file from where commands will be read and path of log file");
                        fromRead = Console.ReadLine();
                        pathLog = Console.ReadLine();
                    }
                    string[] lines = File.ReadAllLines(fromRead);
                    foreach (string s in lines)
                    {
                        string[] cmds = s.Split(' ');
                        switch (cmds.Length)
                        {
                            case 1:
                                shop.DoAction(cmds[0], false, pathLog);
                                break;
                            case 2:
                                shop.DoAction(cmds[0], false, pathLog, cmds[1]);
                                break;
                            case 3:
                                shop.DoAction(cmds[0], false, pathLog, cmds[1], cmds[2]);
                                break;
                        }
                    }
                    status = shop.shop.ToString();
                    StreamWriter sw = new StreamWriter(pathLog, true);
                    sw.WriteLine("Status of System:");
                    sw.Write(status);
                    sw.Close();
                    break;
            }

            Console.ReadLine();
        }
    }
}